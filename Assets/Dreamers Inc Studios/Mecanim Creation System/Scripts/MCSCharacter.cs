﻿using UnityEngine;
using System.Collections;
using System;


namespace DreamersIncStudios.MecanimCreationSystem
{
    [System.Serializable]
    public class MCSCharacter : MCSICharacter
    {
        [SerializeField]
        Animator _anim;
        [SerializeField]
        string _name;
        [SerializeField]
        GameObject _prefab;

        public MCSCharacter(){
            _anim = new Animator();
            _prefab = null;
            _name = "";
            }
        public MCSCharacter(Animator anim, string name, GameObject prefab) {
            _anim = anim;
            _name = name;
            _prefab = prefab;
        }


        public Animator Anim
        {
            get { return _anim; }
            set { _anim = value; }
        }

        public GameObject CharacterModel
        {
            get { return _prefab; }
            set { _prefab = value; }}

        public string CharName
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
