﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace DreamersIncStudios.MecanimCreationSystem
{ 
public class GenericDatabaseCreate<T> : ScriptableObject where T : class
    {
        [SerializeField]
        List<T> Database = new List<T>();

        public void Add(T item) {
            Database.Add(item);
            EditorUtility.SetDirty(this);
        }
        public void Insert(int index, T item) { Database.Insert(index, item);
            EditorUtility.SetDirty(this);
        }
        public void Remove(int index) { Database.RemoveAt(index);
            EditorUtility.SetDirty(this);
        }
        public void Remove(T item) {
            Database.Remove(item);
            EditorUtility.SetDirty(this);
        }
        public int Count {
            get { return Database.Count; }
        }
        public T Get(int index) {
            return Database.ElementAt(index);
        }
        public void Replace(int index, T item) { Database[index] = item;
            EditorUtility.SetDirty(this);
        }
        public U GetDatabase<U>(string dbpath, string dbname) where U : ScriptableObject
        {
            string dbfullpath = @"Assets/" + dbpath + "/" + dbname;

            U db = AssetDatabase.LoadAssetAtPath(dbfullpath, typeof(U)) as U;
            if (db == null)
            {
                if (!AssetDatabase.IsValidFolder("Assets/" + dbpath))
                {
                    AssetDatabase.CreateFolder("Assets", dbpath);
                }
                db = ScriptableObject.CreateInstance<U>();
                AssetDatabase.CreateAsset(db, dbfullpath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
            return db;
        }
    }
}
