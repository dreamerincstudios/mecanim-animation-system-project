﻿using UnityEngine;
using System.Collections;


namespace DreamersIncStudios.MecanimCreationSystem{
    public interface MCSICharacter  {

        string CharName { get; set; }
        GameObject CharacterModel { get; set; }
        Animator Anim { get; set; }
	
	}
}
