﻿using UnityEngine;
using System.Collections;

public partial class ISMain {
  float HoldTimer;// using to determine if button has be held down
   public float HoldReset =.2f;
    float pausetimer;
    public float PauseReset = .2f;
    bool Pressed = false;


    void SetTimers(){
       HoldTimer = HoldReset;
        pausetimer=PauseReset;        }


    void StringConvert() {
       // if (!Pressed) //if  we haven't taking the input for this run
        //{
            if (Input.GetAxis("Horizontal") > 0 && Input.GetAxis("Vertical") == 0)
            { StringWrite("6"); Pressed = true ; }
            if (Input.GetAxis("Horizontal") < 0 && Input.GetAxis("Vertical") == 0)
            { StringWrite("4"); Pressed = true; }
            if (Input.GetAxis("Vertical") > 0 && Input.GetAxis("Horizontal") == 0)
            { StringWrite("8"); Pressed = true; }
            if (Input.GetAxis("Vertical") < 0 && Input.GetAxis("Horizontal") == 0)
            { StringWrite("2"); Pressed = true; }
            if (Input.GetAxis("Horizontal") > 0 && Input.GetAxis("Vertical") > 0)
            { StringWrite("9"); Pressed = true; }
            if (Input.GetAxis("Horizontal") > 0 && Input.GetAxis("Vertical") < 0)
            { StringWrite("3"); Pressed = true; }
            if (Input.GetAxis("Vertical") > 0 && Input.GetAxis("Horizontal") < 0)
            { StringWrite("7"); Pressed = true; }
            if (Input.GetAxis("Vertical") < 0 && Input.GetAxis("Horizontal") <0)
            { StringWrite("1"); Pressed = true; }
            if (Input.GetButtonUp("Fire1"))
                { StringWrite("X"); LastKeyPressed = null; }
        if (Input.GetButtonUp("Fire2"))
        { StringWrite("B"); LastKeyPressed = null; }
        if (Input.GetButtonUp("Fire3"))
        { StringWrite("Y"); LastKeyPressed = null; }
        if (Input.GetButtonUp("Jump"))
        { StringWrite("A"); LastKeyPressed = null; }


        if (Input.GetButtonUp("Fire1"))
            Pressed = true;
        if (Input.GetButtonUp("Fire2"))
            Pressed = true;
        if (Input.GetButtonUp("Fire3"))
            Pressed = true;
        if (Input.GetButtonUp("Jump"))
            Pressed = true;
                        // }

            //Pause
        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0 && !Pressed &&
           pausetimer < 0)
            StringWrite("P");
            if (pausetimer> 0)
            pausetimer -= Time.deltaTime;
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0||Pressed )
            pausetimer = PauseReset;



        //reset pressed check
        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0 && Pressed)
            Pressed = !Pressed;
    
        //Pressed Buttons reset
        if (Input.GetButton("Fire1") && HoldTimer < 0.0f)
            {  StringWrite("XH"); Pressed = true; }
            if (Input.GetButton("Fire1") && HoldTimer > 0) HoldTimer -= Time.deltaTime;
        if (Input.GetButtonUp("Fire1")) { HoldTimer = HoldReset; Pressed = !Pressed; }

        if (Input.GetButton("Fire2") && HoldTimer < 0.0f)
        { StringWrite("BH"); Pressed = true; }
        if (Input.GetButton("Fire2") && HoldTimer > 0) HoldTimer -= Time.deltaTime;
        if (Input.GetButtonUp("Fire2")) { HoldTimer = HoldReset; Pressed = !Pressed; }

        if (Input.GetButton("Fire3") && HoldTimer < 0.0f)
        { StringWrite("YH"); Pressed = true; }
        if (Input.GetButton("Fire3") && HoldTimer > 0) HoldTimer -= Time.deltaTime;
        if (Input.GetButtonUp("Fire3")) { HoldTimer = HoldReset; Pressed = !Pressed; }

        if (Input.GetButton("Jump") && HoldTimer < 0.0f)
        { StringWrite("AH"); Pressed = true; }
        if (Input.GetButton("Jump") && HoldTimer > 0) HoldTimer -= Time.deltaTime;
        if (Input.GetButtonUp("Jump")) { HoldTimer = HoldReset; Pressed = !Pressed; }
    }
}
