﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public partial class ISMain  {
 
   // [SerializeField]
    public List <Combo> AtkCombos;
    
    
}
[System.Serializable]
public class Combo
{
    public string name;
    public List<string> Keys;
    public AnimationClip Move;
    public List<string> NextMove;
}
