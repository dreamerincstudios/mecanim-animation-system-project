﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour
{

    private ISKeyCombo falconPunch = new ISKeyCombo(new string[] { "down", "right", "right" });
    private ISKeyCombo falconKick = new ISKeyCombo(new string[] { "down", "right", "left" });

    void Update()
    {
        if (falconPunch.Check())
        {
            // do the falcon punch
            Debug.Log("PUNCH");
        }
        if (falconKick.Check())
        {
            // do the falcon punch
            Debug.Log("KICK");
        }
    }
}