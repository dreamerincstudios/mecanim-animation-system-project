﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public partial class ISMain 
{
    public List<string> inputSequence;
    string LastKeyPressed;

    void StringWrite(string key) {
        if (LastKeyPressed == null || !LastKeyPressed.Equals(key))
        {
            inputSequence.Add(key);
        }
            if (inputSequence.Count > 10)
                inputSequence = new List<string>();
        LastKeyPressed = key;
    }
}
